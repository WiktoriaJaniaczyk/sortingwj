import fxchart.ArrayUtils;
import fxchart.InsertionSorter;
import fxchart.Sorter;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        for (int i = 0; i < 5000; i++) {
//            long start = System.currentTimeMillis();
            LocalTime start = LocalTime.now();


            int[] ints = ArrayUtils.generateIntegerRandomArray(100);
//            System.out.println(Arrays.toString(ints));

//        Sorter sorter = new BubbleSorter();
            Sorter sorter = new InsertionSorter();
            sorter.sort(ints);

//            System.out.println("Po sortowaniu");
//            System.out.println(Arrays.toString(ints));
//            long end = System.currentTimeMillis();
            LocalTime end = LocalTime.now();
            Duration duration = Duration.between(end, start);


            System.out.println(duration.abs());

        }

    }
}
