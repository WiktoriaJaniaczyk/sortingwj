package fxchart;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.VBox;

import java.util.HashMap;
import java.util.Map;

public class Controller {

    @FXML
    private VBox vBox;

    @FXML
    private LineChart<Integer, Integer> lineChart;

    @FXML
    private Button btnUpdate;

    private Map<String, CheckBox> checkBoxByAlgorithmName;
    private Sorter[] sorters;

    public void initialize(){
        sorters = new Sorter[] {
                new BubbleSorter(),
                new InsertionSorter()
        };

        checkBoxByAlgorithmName = new HashMap<>();
        for (Sorter srt: sorters){
            CheckBox sorterCheckBox =
                    new CheckBox(srt.getAlgorithmName());
            vBox.getChildren().add(sorterCheckBox);
            checkBoxByAlgorithmName.put(
                    srt.getAlgorithmName(),
                    sorterCheckBox
            );
            btnUpdate.setOnAction(event -> updateChart());
        }
    }

    private void updateChart() {
        lineChart.getData().clear();
        for (Sorter srt: sorters){
            if (checkBoxByAlgorithmName
                    .get(srt.getAlgorithmName()).isSelected()){
                ObservableList<XYChart.Data<Integer, Integer>>
                        data = FXCollections.observableArrayList();
                for (int elementCount = 0; elementCount < 10000; elementCount+=500) {
                    int [] arr =
                            ArrayUtils
                                    .generateIntegerRandomArray(elementCount);
                    long startTime = System.currentTimeMillis();
                    srt.sort(arr);
                    long sortedTime =
                            System.currentTimeMillis() - startTime;
                    data.add(
                            new XYChart.Data<>(
                                    elementCount, (int)sortedTime));

                }
                XYChart.Series<Integer,Integer> series =
                        new XYChart.Series<>(srt.getAlgorithmName(), data);
                lineChart.getData().add(series);
            }
        }

    }


}
