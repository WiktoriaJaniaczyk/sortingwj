package fxchart;


public class InsertionSorter implements Sorter {
    //sortowanie przez wstawianie

    @Override
    public void sort(int[] toSort) {
        for (int i = 1; i < toSort.length ; i++) {
            int j = i;
            //jesli uzywamy z petla while to for musi miec zakres N-1
//            while (j > 0 && toSort[j - 1] > toSort[j]) {
//                ArrayUtils.swapElements(toSort, j, j - 1);
//                j--;
//            }
            for (j = i - 1; j >= 0  ; j--) {
                if(toSort[j] > toSort[j+1] ){
                    ArrayUtils.swapElements(toSort, j, j+ 1);
                }else{
                    break;
                }
            }

        }
    }

    @Override
    public String getAlgorithmName() {
        return "Sortowanie przez wstawianie";
    }
}

