package fxchart;
import java.lang.reflect.Array;
import java.util.Random;

public class ArrayUtils {

    public static int[] generateIntegerRandomArray(int length){
        int[] result = new int[length];
        Random random = new Random();
        for (int i = 0; i < result.length; i++) {
            result[i] = random.nextInt(result.length * 5);
        }
        return result;
    }

    public static void swapElements(int[] toSort, int j, int i) {
        //[j, i]
        //[i, j]
        int indexITemp = toSort[i];
//        int indexJ = toSort[j];
        toSort[i] = toSort[j];
        toSort[j] = indexITemp;
     }
}
